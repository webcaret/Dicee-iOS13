//
//  ViewController.swift
//  Dicee-iOS13
//
//  Created by Angela Yu on 11/06/2019.
//  Copyright © 2019 London App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var diceOne: UIImageView!
    @IBOutlet weak var diceTwo: UIImageView!

    let diceValues = [ #imageLiteral(resourceName: "DiceOne"), #imageLiteral(resourceName: "DiceTwo"), #imageLiteral(resourceName: "DiceThree"), #imageLiteral(resourceName: "DiceFour"), #imageLiteral(resourceName: "DiceFive"), #imageLiteral(resourceName: "DiceSix") ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roll(dice: diceOne)
        roll(dice: diceTwo)
    }

    @IBAction func rollButtonTapped(_ sender: UIButton) {
        roll(dice: diceOne)
        roll(dice: diceTwo)
    }
    
    func roll(dice: UIImageView) {
        dice.alpha = 0.1
        dice.image = diceValues.randomElement()
        dice.alpha = 1
    }
    
}

